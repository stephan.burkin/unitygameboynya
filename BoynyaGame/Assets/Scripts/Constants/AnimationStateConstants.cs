﻿namespace ConstantsValue
{
    public static class AnimationStateConstants
    {
        public const string IsIdle = "IsIdle";
        
        public const string IsRoll = "IsRoll";

        public const string IsShieldImpact = "IsShieldImpact";
        
        public const string IsSimpleAttack = "IsSimpleAttack";
        
        public const string IsImpact = "IsImpact";

        public const string IsBlocking = "IsBlocking";
        
        public const string IsDead = "IsDead";

        public const string IsOpening = "IsOpening";
        
        public const string IsTryingToOpen = "IsTryingToOpen";
    }
}