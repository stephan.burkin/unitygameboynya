﻿using UnityEngine;

namespace Hero
{
    public class HeroSongs : MonoBehaviour
    {
        public AudioClip attackSong;
        
        public AudioClip impactSong;
        
        public AudioClip deathSong;
    }
}