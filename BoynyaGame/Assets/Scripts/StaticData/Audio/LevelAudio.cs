using System;
using UnityEngine;

namespace StaticData.Audio
{
    [Serializable]
    public class LevelAudio
    {
        public string LevelName;
        public AudioClip AudioClip;
    }
}