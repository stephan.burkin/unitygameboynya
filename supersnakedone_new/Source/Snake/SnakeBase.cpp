// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakeBase.h"
#include "SnakeElementBase.h"
#include "Interactable.h"
#include "Kismet/GameplayStatics.h"
#include "Engine.h"

// Sets default values
ASnakeBase::ASnakeBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	ElementSize = 100.f;
	Speed = 10.f;
	
}

// Called when the game starts or when spawned
void ASnakeBase::BeginPlay()
{
	Super::BeginPlay();
	LastMovementDirection = EDirection::UP;
	DefaultSpeed = Speed;
	SetActorTickInterval(Speed);
	AddSnakeElement(3);
}

// Called every frame
void ASnakeBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	Move();
}

void ASnakeBase::AddSnakeElement(int Count)
{
	for (int i = 0; i < Count; i++)
	{
		FVector ShiftVector(SnakeElements.Num()*ElementSize, 0, 0);

		if (SnakeElements.Num() > 0)
		{
			ShiftVector = SnakeElements[SnakeElements.Num() - 1]->GetActorLocation();
		}

		FTransform NewTransform = FTransform(ShiftVector);
		auto SnakeElement = GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementClass, NewTransform);

		SnakeElement->SnakeOwner = this;
		int32 index = SnakeElements.Add(SnakeElement);

		if (index == 0)
		{
			SnakeElement->SetHeadElementType();
		}
	}
}

//void ASnakeBase::AddSnakeElement(int Count)
//{
//	if (Count == 1)
//	{
//		LastElementTransform.SetScale3D(FVector(1, 1, 1));
//		auto SnakeElement = GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementClass, LastElementTransform);
//		SnakeElement->SnakeOwner = this;
//		int32 index = SnakeElements.Add(SnakeElement);
//
//		if (index == 0)
//		{
//			SnakeElement->SetHeadElementType();
//		}
//	}
//	else
//	{
//		for (int i = 0; i < Count; i++)
//		{
//			FVector ShiftVector(SnakeElements.Num() * ElementSize, 0, 0);
//			FTransform NewTransform = FTransform(GetActorLocation() - ShiftVector);
//			LastElementTransform.SetScale3D(FVector(1, 1, 1));
//			auto SnakeElement = GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementClass, NewTransform);
//			SnakeElement->SnakeOwner = this;
//			int32 index = SnakeElements.Add(SnakeElement);
//
//			if (index == 0)
//			{
//				SnakeElement->SetHeadElementType();
//			}
//		}
//	}
//}

		

void ASnakeBase::Move()
{
	const float MovementDelta = ElementSize;
	FVector MovementVector(ForceInitToZero);

	switch (LastMovementDirection)
	{
	case EDirection::UP:
		MovementVector.X += MovementDelta;
		break;
	case EDirection::DOWN:
		MovementVector.X -= MovementDelta;
		break;
	case EDirection::LEFT:
		MovementVector.Y -= MovementDelta;
		break;
	case EDirection::RIGHT:
		MovementVector.Y += MovementDelta;
		break;
	default:
		MovementVector.X += MovementDelta;
		break;
	}

	//FVector NewHeadLocation = SnakeElements[0]->GetActorLocation() + MovementVector;
	//SnakeElements[0]->SetActorLocation(NewHeadLocation);
	SnakeElements[0]->ToggleCollision();

	//LastElementTransform = SnakeElements.Last()->GetTransform();

	//for (int i = 1; i < SnakeElements.Num(); i++) // �������� �������
	for(int i = SnakeElements.Num() - 1; i > 0; i--)
	{
		auto Element = SnakeElements[i];
		auto PrevElement = SnakeElements[i - 1];
		FVector PrevLocation = PrevElement->GetActorLocation();
		Element->SetActorLocation(PrevLocation);
	}

	SnakeElements[0]->AddActorWorldOffset(MovementVector);
	SnakeElements[0]->ToggleCollision();
	Moving = false;
}



void ASnakeBase::SnakeElementOverlap(ASnakeElementBase* OverlappedElement, AActor* Actor)
{
	if(IsValid(OverlappedElement))
	{
		//bool IsHead = OverlappedElement == SnakeElements[0];
	    //if (!IsHead) return;
		IInteractable* Interactable = Cast<IInteractable>(Actor);
		if (Interactable != nullptr)
		{
			Interactable->Interact(this);
		}
		else
		{
			const FString CurrentLevelNameString = GetWorld()->GetName();
			const FName CurrentLevelName(CurrentLevelNameString);
			UGameplayStatics::OpenLevel(this, CurrentLevelName, false);
		}
	}
}

void ASnakeBase::IncreaseSpeed(float SpeedMultiplier, float Time)
{
	if (SpeedTimer.IsValid()) GetWorldTimerManager().ClearTimer(SpeedTimer);
	Speed /= SpeedMultiplier;
	SetActorTickInterval(Speed);
	GetWorldTimerManager().SetTimer(SpeedTimer, this, &ASnakeBase::ReturnSpeed, 1.0f, false, Time);
}

void ASnakeBase::ReturnSpeed()
{
	Speed = DefaultSpeed;
	SetActorTickInterval(Speed);
	GetWorldTimerManager().ClearTimer(SpeedTimer);
}
